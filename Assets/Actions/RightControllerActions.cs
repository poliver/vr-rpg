﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Actions
{
    public class RightControllerActions : MonoBehaviour, XRIDefaultInputActions.IXRIRightHandActions
    {
        private XRIDefaultInputActions controls;
        [SerializeField] private Player player;
        [SerializeField] private float grabRadius = 0.5f;

        private void OnEnable()
        {
            if (controls == null)
            {
                controls = new XRIDefaultInputActions();
                controls.XRIRightHand.SetCallbacks(this);
            }
            controls.XRILeftHand.Enable();
        }

        public void OnPosition(InputAction.CallbackContext context)
        {
        }

        public void OnRotation(InputAction.CallbackContext context)
        {
        }

        public void OnSelect(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                var hits = Physics.OverlapSphere(transform.position, grabRadius, 1 << LayerMask.NameToLayer("Item"));
                foreach (var hit in hits)
                {
                    var item = hit.GetComponent<Item>();
                    player.Inventory.Add(item);
                }
            }
        }

        public void OnActivate(InputAction.CallbackContext context)
        {
        }

        public void OnUIPress(InputAction.CallbackContext context)
        {
        }

        public void OnHapticDevice(InputAction.CallbackContext context)
        {
        }

        public void OnTeleportSelect(InputAction.CallbackContext context)
        {
        }

        public void OnTeleportModeActivate(InputAction.CallbackContext context)
        {
        }

        public void OnTeleportModeCancel(InputAction.CallbackContext context)
        {
        }

        public void OnTurn(InputAction.CallbackContext context)
        {
        }

        public void OnMove(InputAction.CallbackContext context)
        {
        }

        public void OnRotateAnchor(InputAction.CallbackContext context)
        {
        }

        public void OnTranslateAnchor(InputAction.CallbackContext context)
        {
        }

        public void OnPrimaryButtonPress(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Debug.Log("[Right controller]: Primary button pressed!");
            }
        }

        public void OnSecondaryButtonPress(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Debug.Log("[Right Controller]: Secondary button pressed!");
            }
        }
    }
}